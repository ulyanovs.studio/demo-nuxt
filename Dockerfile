FROM node:carbon AS base

RUN mkdir -p /root/apps
COPY . /root/apps

WORKDIR /root/apps

COPY package*.json ./

RUN npm ci && npm i -g pm2

ENV NODE_ENV=production

COPY . /root/apps
RUN npm run build

ENV HOST 0.0.0.0
EXPOSE 3000

CMD pm2 startOrRestart ecosystem.json --no-daemon
