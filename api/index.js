const app = require('express')()
const consola = require('consola')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const host = process.env.HOST || '127.0.0.1'
const port = process.env.PORT || 5000
const helmet = require('helmet')
require('./v1/models/index')
const routes = require('./v1/routes')

// setting various HTTP headers
app.use(helmet())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(bodyParser.text())
app.use(cookieParser())

app.use(routes)

// error processing
app.use((req, res, next) => {
  res.status(404).json({ err: `404\nNot found` })
})

app.use((err, req, res, next) => {
  console.log(err.stack)
  res.status(500).json({ err: '500\nServer error' })
})

app.set('port', port)

// Import and Set Nuxt.js options
let config = require('../nuxt.config.js')
config.dev = !(process.env.NODE_ENV === 'production')

// Listen the server
app.listen(port, host)
consola.ready({
  message: `Server listening on http://${host}:${port}`,
  badge: true
})
