const uuidv1 = require('uuid/v1')
const uuidv4 = require('uuid/v4')
const schema = require('./schema')

// start of use mongoose DB SCHEME
const schemaUsers = schema.User

/*\*\*\*\*\* USERS methods *\*\*\*\*\*\*/

// verifying the validity of the USER
const isNotValidUser = data => {
  return !data.email || !data.password || !data.role ? false : true
}

// GET users of DB
module.exports.getUsers = function() {
  return schemaUsers.find({}, { password: false, __v: false })
}

// GET user of DB
module.exports.getUser = function(data) {
  return schemaUsers.findOne({ _id: data }, { password: false, __v: false })
}

// SAVE new user in DB
module.exports.saveNewUser = function(data) {
  if (!isNotValidUser(data)) {
    return Promise.reject(new Error('email, role, password is required!'))
  }
  const User = new schemaUsers({
    access_token: uuidv1(),
    email: data.email,
    password: data.password,
    role: data.role,
    created_at: +new Date()
  })
  return User.save()
}

// DELETE user of DB
module.exports.deleteUser = function(data) {
  return schemaUsers.findOneAndRemove({ _id: data })
}

// UPDATE user in DB
module.exports.updateUser = function(data, user, id) {
  const User = {}
  User.email = data.email ? data.email : user.email
  User.password = data.password ? data.password : user.password
  User.role = data.role ? data.role : user.role
  User.updated_at = +new Date()
  return schemaUsers.findOneAndUpdate(
    {
      _id: id
    },
    {
      $set: User
    },
    { new: true }
  )
}
