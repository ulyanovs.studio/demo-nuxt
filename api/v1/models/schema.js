const mongoose = require('mongoose')
const Schema = mongoose.Schema

// all data types
const types = {
  username: {
    type: String,
    required: [true, 'username is required'],
    unique: true,
    minlength: [3, 'username too short, min 3'],
    maxlength: [20, 'username too long, max 20']
  },
  text: {
    type: String,
    required: false,
    maxlength: [250, 'too long, max 250']
  },
  longtext: {
    type: String,
    required: false,
    minlength: [3, 'too short, max 3'],
    maxlength: [500, 'too long, max 500']
  },
  password: {
    type: String,
    required: [true, 'password is required'],
    minlength: [3, 'too short, min 3'],
    maxlength: [250, 'too long, max 250']
  }
}

// data userScheme types
const userScheme = new Schema({
  access_token: types.text,
  email: { type: String, required: [true, 'email is required'], unique: true },
  password: types.password,
  role: { type: String, required: [true, 'role is required'] },
  created_at: { type: Number },
  updated_at: { type: mongoose.Mixed, unique: false }
})

// exports Scheme models
module.exports.User = mongoose.model('users', userScheme)
