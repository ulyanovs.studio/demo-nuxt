const { Router } = require('express')
const router = Router()

const ctrlUser = require('../controllers/user')
const baseUrl = '/api/v1'

/* GET users listing. */
router
  .get(`${baseUrl}/users`, ctrlUser.getUsers)
  .post(`${baseUrl}/users`, ctrlUser.saveNewUser)
  .delete(`${baseUrl}/users`, ctrlUser.deleteUser)

/* GET user by ID. */
router
  .get(`${baseUrl}/users/:id`, ctrlUser.getUser)
  .patch(`${baseUrl}/users/:id`, ctrlUser.updateUser)

router.post(`${baseUrl}/login`, ctrlUser.login)
router.post(`${baseUrl}/authFromToken`, ctrlUser.authFromToken)

module.exports = router
