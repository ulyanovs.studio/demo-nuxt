const db = require('../models/db')
const passwordLibs = require('../libs/passwordBcrypt')
const formidable = require('formidable')
const schema = require('../models/schema')
const path = require('path')
const fs = require('fs')

// start of use mongoose DB SCHEME
const schemaUsers = schema.User

// password operations
const createHash = passwordLibs.createHash
const isValidPassword = passwordLibs.isValidPassword

// controllers for POST request /api/v~/login
module.exports.login = function(req, res) {
  let bodyObj = req.body
  schemaUsers
    .findOne({ email: bodyObj.email })
    .then(user => {
      if (user && isValidPassword(user.password, bodyObj.password)) {
        if (bodyObj.remembered) {
          res.cookie('access_token', user.access_token, { maxAge: 360000000 })
        }
        db.getUser(user._id).then(item => {
          res.status(200).json(item)
        })
      } else {
        res.status(400).json({ error: 'user not found' })
      }
    })
    .catch(() => {
      res.status(400).json({
        error: 'user not found! check email or password.'
      })
    })
}

// controllers for POST auto request /api/authFromToken
// replaces the old token with a new one
// for authentication
module.exports.authFromToken = function(req, res) {
  const tokenObj = JSON.parse(req.body)
  schemaUsers
    .findOne(
      { access_token: tokenObj.access_token },
      { password: false, __v: false }
    )
    .then(user => {
      if (user) {
        res.status(200).json(user)
      } else {
        res.json({ error: 'token not found' })
      }
    })
    .catch(err => {
      res.status(400).json({ error: err.message })
    })
}

// controllers for GET request /api/v~/users
module.exports.getUsers = function(req, res) {
  db.getUsers()
    .then(results => {
      res.json(results)
    })
    .catch(err => {
      res.status(400).json({ error: err.message })
    })
}

// controllers for GET request /api/v~/users/:id
module.exports.getUser = function(req, res) {
  db.getUser(req.params.id)
    .then(results => {
      if (results) {
        res.json(results)
      } else {
        res.status(400).json({ error: 'user not found' })
      }
    })
    .catch(err => {
      res.status(400).json({ err: err.message })
    })
}

// controllers for POST request /api/v~/users
module.exports.saveNewUser = function(req, res) {
  const bodyObj = req.body
  bodyObj.password
    ? (bodyObj.password = createHash(bodyObj.password))
    : (bodyObj.password = null)

  schemaUsers
    .findOne({ email: bodyObj.email })
    .then(user => {
      if (user) {
        res.json({ error: 'user with this login already exists' })
      } else {
        db.saveNewUser(bodyObj).then(results => {
          db.getUser(results._id).then(user => {
            res.status(201).json(user)
          })
        })
      }
    })
    .catch(err => {
      res.status(400).json({ error: err.message })
    })
}

// controllers for DELETE request /api/v~/users/:id
module.exports.deleteUser = function(req, res) {
  db.deleteUser(req.query.id)
    .then(results => {
      if (results) {
        res.json(results)
      } else {
        res.status(400).json({ error: 'user not found' })
      }
    })
    .catch(err => {
      res.status(400).json({ err: err.message })
    })
}

// controllers for PUT request /api/updateUser/:id
module.exports.updateUser = function(req, res) {
  const bodyObj = req.body
  schemaUsers
    .findOne({ _id: req.params.id })
    .then(user => {
      if (user) {
        if (bodyObj.oldPassword) {
          if (isValidPassword(user.password, bodyObj.oldPassword)) {
            bodyObj.password = createHash(bodyObj.password)
          } else {
            res.status(400).json({ error: 'password incorrect' })
          }
        }
        db.updateUser(bodyObj, user, req.params.id).then(results => {
          if (results) {
            db.getUser(results._id).then(item => {
              res.json(item)
            })
          } else {
            res.status(400).json({ error: 'user not found' })
          }
        })
      } else {
        res.status(400).json({ error: 'undefined user' })
      }
    })
    .catch(err => {
      res.status(400).json({ error: err.message })
    })
}
